#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>

static const char *dirpath = "/home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data";
static const char *logpath = "/home/vagrant/Sisop/modul4/logmucatatsini.txt";

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
	
	char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

	// Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
	if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	        
	        fprintf(logFile, "FAILED::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	        
	        fclose(logFile);
	    }
	}else{
		res = mkdir(finalpath, mode);
	    FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	
	        fclose(logFile);
	    }
	    if (res == -1) return -errno;
	}
	
	
	
    return 0;
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    int res;
	
	char foldpath[2000];
	sprintf(foldpath, "%s%s", dirpath, oldpath);
	
	char fnewpath[2000];
	sprintf(fnewpath, "%s%s", dirpath, newpath);
	
	char* resPosition = strstr(foldpath, "restricted");
	char* pasPosition = strstr(fnewpath, "bypass");
	
	// Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
    // Check if the old path contains the word "restricted" dan tidak ada bypass
    if (strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") == NULL) {
        // Check if the new path contains the word "bypass"
        if (strstr(fnewpath, "bypass") == NULL) {
        	
            fprintf(stderr, "Error: Renaming a restricted file or folder is not allowed.\n");
            
			//tulis log failed
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
        }else{
        	res = rename(foldpath, fnewpath);
        	
        	//tulis log success
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
    }else if(strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") != NULL){
		if(resPosition > pasPosition){
			
			//tulis log failed
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
		}else{
			res = rename(foldpath, fnewpath);
        	
        	//tulis log success
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
	}else{
    	res = rename(foldpath, fnewpath);
    	
    	//tulis log success
    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
	        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
	        fclose(logFile);
		}
	    if (res == -1) return -errno;
	}
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);
    
    // Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
    
    // Mendapatkan jenis path (file atau folder)
    struct stat st;
    if (lstat(finalpath, &st) == -1) return -errno;

    const char *function;
    const char *function2;
    if (S_ISDIR(st.st_mode)){
        function = "RMDIR";
        function2 = "directory";
    }else{
        function = "RMFILE";
        function2 = "file";
	}
	
    // Check if the path contains the word "restricted"
    if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
        fprintf(stderr, "Error: Deleting a restricted file or folder is not allowed.\n");
        
        //tulis log failed remove
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "FAILED::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
		return -EINVAL;
    }else{
    	res = unlink(finalpath);
    	
    	//tulis log success remove
    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
    	if (res == -1) return -errno;
	}

    
    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

    res = lstat(finalpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static struct fuse_operations xmp_oper = {
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .getattr = xmp_getattr,
};

int main(int argc, char *argv[]){
	
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
    
}
