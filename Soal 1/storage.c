#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define MAX_FIELDS 15

int checkplayer(char** fields){
    int age = atoi(fields[2]);
    int potential = atoi(fields[7]);
    char klub[128];
    strcpy(klub, fields[8]);

    if(age < 25 && potential > 85 && strcmp(klub, "Manchester City")){
        return 1;
    }
    
    else return 0;
}

void printplayer(char** fields){
    printf("Nama: %s\n", fields[1]);
    printf("Umur: %s\n", fields[2]);
    printf("URL Foto: %s\n", fields[3]);
    printf("Kebangsaan: %s\n", fields[4]);
    printf("Potensi: %s\n", fields[7]);
    printf("Klub: %s\n", fields[8]);
    printf("Harga: %s\n", fields[10]);
    printf("Gaji: %s\n", fields[11]);
    printf("\n");
}

int main() {
    
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip -n fifa-player-stats-database.zip -d .");

    FILE *fp;

    int find_count = 1;
    char temp[1024];

    if((fp = fopen("FIFA23_official_data.csv", "r")) == NULL){ // Membuka 
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){ // Membandingkan dengan input
        char *field = strtok(temp, ",");
        char *fields[MAX_FIELDS];
        int field_count = 0;

        while(field != NULL && field_count < MAX_FIELDS){
            fields[field_count] = field;
            field = strtok(NULL, ",");
            field_count++;
        }

        if(checkplayer(fields)){
            printf("Pemain ke-%d\n", find_count);
            printplayer(fields);
            find_count++;    
        }
    }

    if(fp){
        fclose(fp);
    }

}