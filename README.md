# sisop-praktikum-modul-4-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)


## Soal 1
Di soal ini terdapat dataset berupa pemain fifa yang dapat didownload dengan menggunakan command `kaggle datasets download -d bryanb/fifa-player-stats-database`. Karena download ini menggunakan api kaggle, maka perlu menginstal kaggle terlebih dahulu. Berikut adalah command untuk menginstal kaggle:

```
sudo apt-get install -y python3 \
     python3-pip

pip install kaggle
```
Selanjutnya kita perlu mendownload token api kaggle dari websitenya. Token didownload dengan membuka aturan **settings** pada profil akun kaggle.

![image](https://github.com/barpeot/img-modul/blob/main/kaggle-api2.png?raw=true)

# storage.c

Storage.c merupakan program c yang digunakan untuk mendownload dataset dan melakukan unzip pada file **FIFA23_official_data.csv**. Setelah dilakukan unzip, program ini akan mengoutput semua pemain yang memenuhi ketentuan soal, yaitu berusia dibawah **25** tahun, memiliki potensi diatas **85**, dan memiliki klub selain **Manchester City**.

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define MAX_FIELDS 15

int main() {
    
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip -n fifa-player-stats-database.zip -d .");
```
Program dimulai dengan memanggil system() pada command `kaggle datasets download -d bryanb/fifa-player-stats-database` untuk melakukan download file, kemudian dilanjutkan dengan unzip file tersebut di current direktori.
```
    FILE *fp;

    int find_count = 1;
    char temp[1024];

    if((fp = fopen("FIFA23_official_data.csv", "r")) == NULL){ // Membuka 
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){ // Membandingkan dengan input
        char *field = strtok(temp, ",");
        char *fields[MAX_FIELDS];
        int field_count = 0;

        while(field != NULL && field_count < MAX_FIELDS){
            fields[field_count] = field;
            field = strtok(NULL, ",");
            field_count++;
        }

        if(checkplayer(fields)){
            printf("Pemain ke-%d\n", find_count);
            printplayer(fields);
            find_count++;    
        }
    }

    if(fp){
        fclose(fp);
    }

}
```
Selanjutnya, program akan melakukan fopen dari file **FIFA23_official_data.csv** kemudian untuk setiap field di setiap line pada file tersebut akan dimasukkan ke dalam array fields[] sesuai dengan urutan fieldnya. Di program ini field yang diambil adalah sebanyak 15 fields. Ini dilakukan untuk setiap line pada file **FIFA23_official_data.csv**.
```
int checkplayer(char** fields){
    int age = atoi(fields[2]);
    int potential = atoi(fields[7]);
    char klub[128];
    strcpy(klub, fields[8]);

    if(age < 25 && potential > 85 && strcmp(klub, "Manchester City")){
        return 1;
    }
    
    else return 0;
}

void printplayer(char** fields){
    printf("Nama: %s\n", fields[1]);
    printf("Umur: %s\n", fields[2]);
    printf("URL Foto: %s\n", fields[3]);
    printf("Kebangsaan: %s\n", fields[4]);
    printf("Potensi: %s\n", fields[7]);
    printf("Klub: %s\n", fields[8]);
    printf("Harga: %s\n", fields[10]);
    printf("Gaji: %s\n", fields[11]);
    printf("\n");
}
```
Selanjutnya dilakukan pengecekan dengan menggunakan fungsi checkplayer(), untuk umur disimpan di fields[2], potensi disimpan di fields[7] dan klub disimpan di fields[8]. Apabila checkplayer() berhasil maka akan memanggil fungsi printplayer() untuk mengoutputkan player tersebut. Berikut adalah output dari program:

![image](https://github.com/barpeot/img-modul/blob/main/storagec-output2.png?raw=true)

# Dockerfile
Langkah selanjutnya adalah membuat sebuah image yang dapat menjalankan storage.c. Image ini akan dibuild dengan bantuan **Dockerfile**. Berikut adalah isi dari file **Dockerfile**
```
FROM ubuntu:20.04

RUN apt-get update 

# Install python3 dan pip
RUN apt-get install -y python3 \
    python3-pip

# Dari pip menginstall kaggle
RUN pip install kaggle

# Copy kaggle.json ke image
COPY kaggle.json /root/.kaggle/kaggle.json

# Ubah permission kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json

# Install unzip dan gcc
RUN apt-get install -y unzip \
    gcc

# Copy storage.c ke image
COPY storage.c /app/

# Pindah workdir ke /app
WORKDIR /app

# Compile storage.c
RUN gcc -o storage /app/storage.c

# Run program
CMD ["./storage"]
```
Dari kode diatas dapat dilihat bahwa image akan menggunakan base ubuntu 20.04 dan dimulai dengan menginstall dependencies yang dibutuhkan berupa python3, pip, dan kaggle (option -y digunakan untuk konfirmasi "yes" pada setiap instalasi). Selanjutnya perlu melakukan copy kaggle.json dari host machine ke /root/.kaggle/kaggle.json agar dapat menggunakan kaggle.

Selanjutnya, perlu menginstall unzip, gcc, dan mengcopy storage.c dari host machine ke direktori /app/. Lalu pindah direktori ke /app/ di image dan melakukan kompilasi dengan gcc dan run program dengan `./storage`.

Setelah Dockerfile dibuat, maka akan dilakukan build image dengan cara:
```
docker build -t storage-app <alamat Dockerfile>
```

Sedangkan untuk melihat image yang ada dapat menggunakan `docker image ls`.

Setelah selesai build image, maka dapat dirun dengan command `docker run <nama image>`.

# hubdocker.txt

Langkah selanjutnya adalah melakukan push docker image ke docker hub. Untuk melakukan push pertama-tama kita perlu login ke docker hub dengan menggunakan command `docker login` dan mengisi username serta password. Selanjutnya kita perlu merename tag menggunakan docker tag.
```
docker tag storage-app <username>/storage-app
```
Selanjutnya kita hanya perlu melakukan push dengan menggunakan docker push.
```
docker push <username>/storage-app
```
![image](https://github.com/barpeot/img-modul/blob/main/dockerhub.png?raw=true)

# docker-compose.yaml

Docker compose digunakan untuk menjalankan banyak banyak instance dari image yang dispesifikasi. Untuk ketentuan di soal 1, diperlukan untuk menjalankan 5 instance dari image storage-app di folder barcelona dan napoli. Berikut adalah isi dari docker-compose.yaml.

```
version: '3'

services:
  storage-app1:
    image: barpeot/storage-app
    ports:
    - "8000:8000"
  storage-app2:
    image: barpeot/storage-app
    ports:
    - "8080:8080"
  storage-app3:
    image: barpeot/storage-app
    ports:
    - "8081:8081"
  storage-app4:
    image: barpeot/storage-app
    ports:
    - "8082:8082"
  storage-app5:
    image: barpeot/storage-app
    ports:
    - "8083:8083"
```
version merupakan spesifikasi versi dari docker compose yang digunakan, untuk instance yang dijalankan ada 5 yaitu storage-app1 hingga 5. Untuk setiap instance yang dijalankan adalah image yang telah dibuat dan dengan menggunakan port yang berbeda-beda.

Untuk menjalankan docker compose ini dapat dengan cara mengcopy file docker-compose.yaml ke folder napoli dan barcelona kemudian menjalankan command `docker compose up` di kedua folder tersebut. Perlu diketahui bahwa jika kita ingin menjalankan docker compose di folder napoli dan barcelona secara bersama-sama, maka nilai dari ports perlu diganti agar tidak ada yang sama.

![image](https://github.com/barpeot/img-modul/blob/main/docker-compose2.png?raw=true)
![image](https://github.com/barpeot/img-modul/blob/main/docker-compose-end.png?raw=true)

## Soal 2

# germa.c
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
```
Pertama include semua library yang akan digunakan
```
static const char *dirpath = "/home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data";
static const char *logpath = "/home/vagrant/Sisop/modul4/logmucatatsini.txt";
```
Selanjutnya saya mendeclare path dari source mount nya dan path dari file log.txt
```
static int xmp_mkdir(const char *path, mode_t mode){
    int res;
	
	char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

	// Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
	if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	        
	        fprintf(logFile, "FAILED::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	        
	        fclose(logFile);
	    }
	}else{
		res = mkdir(finalpath, mode);
	    FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	
	        fclose(logFile);
	    }
	    if (res == -1) return -errno;
	}	
    return 0;
}
```
Selanjutnya adalah fungsi untuk mkdir. Disini saya pertama menggabungkan path source dan path input agar mendapat final path, lalu mendapatkan informasi user dengan getUID, lalu akan di cek apakah path tersebut mengandung kata restricted dan tidak memiliki kata bypass maka artinya tidak bisa mkdir dan akan menulis failed ke log. Di saat menulis log saya mendapatkan waktu sekarang menggunakan time. Selanjutnya jika tidak ada kata restricted maka artinya boleh mkdir lalu tulis log success.
```

static int xmp_rename(const char *oldpath, const char *newpath)
{
    int res;
	
	char foldpath[2000];
	sprintf(foldpath, "%s%s", dirpath, oldpath);
	
	char fnewpath[2000];
	sprintf(fnewpath, "%s%s", dirpath, newpath);
	
	char* resPosition = strstr(foldpath, "restricted");
	char* pasPosition = strstr(fnewpath, "bypass");
	
	// Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
    // Check if the old path contains the word "restricted" dan tidak ada bypass
    if (strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") == NULL) {
        // Check if the new path contains the word "bypass"
        if (strstr(fnewpath, "bypass") == NULL) {
        	
            fprintf(stderr, "Error: Renaming a restricted file or folder is not allowed.\n");
            
			//tulis log failed
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
        }else{
        	res = rename(foldpath, fnewpath);
        	
        	//tulis log success
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
    }else if(strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") != NULL){
		if(resPosition > pasPosition){
			
			//tulis log failed
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
		}else{
			res = rename(foldpath, fnewpath);
        	
        	//tulis log success
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
	}else{
    	res = rename(foldpath, fnewpath);
    	
    	//tulis log success
    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
	        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
	        fclose(logFile);
		}
	    if (res == -1) return -errno;
	}
    return 0;
}
```
Selanjutnya fungsi untuk rename. Pertama gabungkan path dengan oldpath dan newpath, lalu mendapatkan informasi user dengan gerUID, lalu saya mendapatkan (jika ada) lokasi dari kata restricted dan bypass. Setelah itu cek path jika pada old path ada kata restricted dan pada newpath tidak ada kata bypass maka akan failed, namun jika tidak maka akan success rename, selanjutnya cek jika pada  newpath tidak ada kata bypass cek apakah oldpath ada kata restricted dan ada kata bypas, maka cek lokasi dari kata restricted dan bypass tersebut, jika kata restricted lebih dalam dari kata bypass maka akan failed karena file restricted lebih dalam dari bypass jika tidak begitu maka akan success, selanjutnya else terakhir jika bukan semua diatas artinya tidak ada kata restricted maka akan success.
```
static int xmp_unlink(const char *path)
{
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);
    
    // Mendapatkan informasi pengguna (user)
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
    
    // Mendapatkan jenis path (file atau folder)
    struct stat st;
    if (lstat(finalpath, &st) == -1) return -errno;

    const char *function;
    const char *function2;
    if (S_ISDIR(st.st_mode)){
        function = "RMDIR";
        function2 = "directory";
    }else{
        function = "RMFILE";
        function2 = "file";
	}
	
    // Check if the path contains the word "restricted"
    if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
        fprintf(stderr, "Error: Deleting a restricted file or folder is not allowed.\n");
        
        //tulis log failed remove
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "FAILED::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
		return -EINVAL;
    }else{
    	res = unlink(finalpath);
    	
    	//tulis log success remove
    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
    	if (res == -1) return -errno;
	}

    
    return 0;
}
```
Selanjutnya untuk fungsi remove file dan folder pertama gabungkan path, lalu dapatkan informasi user, lalu cek apakah yang dihapus adalah file atau direktori sebagai format nanti saat menulis log, selanjutnya cek path jika ada kata restricted dan tidak ada kata bypass maka akan failed, namun jika bukan itu maka akan success.
```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

    res = lstat(finalpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}
```
Selanjutnya adalah fungsi untuk read dimana menggabungkan path lalu akan read.
```
static struct fuse_operations xmp_oper = {
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .getattr = xmp_getattr,
};
```
Lalu 
```
int main(int argc, char *argv[]){
	
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
    
}
```
Lalu terakhir lakukan return fuse pada main.

# logmucatatsini.txt

jika sudah selanjutnya saya melakukan apa yang diminta pada soal dan mendapatkan hasil log sebagain berikut :

FAILED::2023/05/27-10:25:21::MKDIR::[vagrant] Create /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/products/restricted_list/productMagang

FAILED::2023/05/27-10:25:38::MKDIR::[vagrant] Create /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/restricted_list/projectMagang

SUCCESS::2023/05/27-10:26:37::RENAME::[vagrant] Rename from /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/restricted_list to /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list

SUCCESS::2023/05/27-10:26:56::MKDIR::[vagrant] Create /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list/projectMagang

SUCCESS::2023/05/27-10:28:51::RENAME::[vagrant] Rename from /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list/filePenting to /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting

FAILED::2023/05/27-10:29:15::RENAME::[vagrant] Rename from /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting/ourProject.txt to /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting/ubahnama.txt

SUCCESS::2023/05/27-10:30:52::RENAME::[vagrant] Rename from /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/others/restrictedFileLama to /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/others/bypassFileLama

SUCCESS::2023/05/27-10:31:09::RMFILE::[vagrant] Remove file /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/others/bypassFileLama/fileLama1.txt

SUCCESS::2023/05/27-10:31:13::RMFILE::[vagrant] Remove file /home/vagrant/Sisop/modul4/file_germa/nanaxgerma/src_data/others/bypassFileLama/fileLama2.txt

# soal 3

## secretadmirer.c

```
#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <libgen.h>

#ifndef S_IFREG
#define S_IFREG 0100000 // Regular file bit mask (octal representation)
#endif

#define MAX_PATH 4096

#define MAX_PASSWORD_LENGTH 100

char PASSWORD[MAX_PASSWORD_LENGTH] = "mypass";


char SOURCE_DIR[MAX_PATH];
char MOUNT_POINT[MAX_PATH];

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

```

Pertama include library dan variabel absolut dan global yang akan digunakan.

```
char* base64_encode(const unsigned char* data, size_t inputLen) {
    printf("Former Name: %s\n", data);
    size_t output_length = 4 * ((inputLen + 2) / 3);
    char* hasilEncode = (char*)malloc(output_length + 1); 

    size_t i, j;
    for (i = 0, j = 0; i < inputLen; i += 3, j += 4) {
        unsigned char octet_a = i < inputLen ? data[i] : 0;
        unsigned char octet_b = i + 1 < inputLen ? data[i + 1] : 0;
        unsigned char octet_c = i + 2 < inputLen ? data[i + 2] : 0;

        hasilEncode[j] = base64_table[octet_a >> 2];
        hasilEncode[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        hasilEncode[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        hasilEncode[j + 3] = base64_table[octet_c & 63];
    }

    if (inputLen % 3 == 1) {
        hasilEncode[output_length - 2] = '=';
        hasilEncode[output_length - 1] = '=';
    } else if (inputLen % 3 == 2) {
        hasilEncode[output_length - 1] = '=';
    }

    hasilEncode[output_length] = '\0';
    printf("Encoded Name: %s\n", hasilEncode);
    return hasilEncode;
}
```

Berikutnya terdapat fungsi encode base64 yang akan merubah isi file menjadi terencode base64. Konten sebelum dan sesudah encode akan diprint pada terminal.

```
static  int  xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[MAX_PATH*2];

    sprintf(fpath,"%s%s",SOURCE_DIR,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```

selanjutnya fungsi xmp_getattr untuk mendapat informasi entry ketika proses xmp_readdir. xmp_getattr ini menjembatani antara entry pada mount point dan entry pada mount source FUSE yang telah dibuat.

```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    (void) offset;
    (void) fi;
    dp = opendir(fpath);
    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char filter_name[MAX_PATH];
        memset(filter_name, 0, sizeof(filter_name));

        strcpy(filter_name, de->d_name);

        // kondisi LUTH
        if (filter_name[0] == 'L' || filter_name[0] == 'l' ||
            filter_name[0] == 'U' || filter_name[0] == 'u' ||
            filter_name[0] == 'T' || filter_name[0] == 't' ||
            filter_name[0] == 'H' || filter_name[0] == 'h') {
            
            char* encoded = base64_encode((unsigned char*)filter_name, strlen(filter_name));
            snprintf(filter_name, sizeof(filter_name), "%s", encoded);
            printf("Convert Name: %s\n", filter_name);
            free(encoded);
        }

        // kondisi < 5 ch
        if (strlen(filter_name) < 5) {
            printf("Less than 5: %s\n", filter_name);

            char new_conv[MAX_PATH];
            memset(new_conv, 0, sizeof(new_conv));
            for (size_t i = 0; i < strlen(filter_name); i++) {
                char binary[10];
                binary[8] = ' ';
                binary[9] = '\0';

                unsigned char ch = filter_name[i];
                for (int j = 7; j >= 0; j--) {
                    binary[j] = (ch & 1) + '0';
                    ch >>= 1;
                }

                strcat(new_conv, binary);
            }
            memset(filter_name, 0, sizeof(filter_name));
            strcpy(filter_name, new_conv);
            printf("After convert 5: %s\n\n", filter_name);
        }

        if (S_ISREG(st.st_mode)) {

            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = tolower(filter_name[i]);
            }

            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);

            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);

            rename(old_dir_path, new_dir_path);
        }
        else{

            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = toupper(filter_name[i]);
            }

            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);

            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);

            rename(old_dir_path, new_dir_path);
        }
        
        if (S_ISDIR(st.st_mode)) {

            char new_path[MAX_PATH*2];
            snprintf(new_path, sizeof(new_path), "%s/%s", path, filter_name);
            xmp_readdir(new_path, buf, filler, offset, fi);
        }

        filler(buf, filter_name, &st, 0);
    }

    closedir(dp);
    return 0;
}
```

Berikutnya xmp_readdir, fungsi ini sudah dicustom sedemikian sehingga memenuhi beberapa aturan berikut:

1. Jika entry berawalan 'L', 'U', 'T', 'H' (baik lowercase atau uppercase) maka entry tersebut akan diencode dengan memanggil base64_encode.

2. Jika jumlah character nama entry kurang dari 5 maka akan diubah menjadi binary ASCII 8bit. Perubahan menjadi binary ASCII dilakukan di dalam proses fungsi xmp_readdir.

3. jika entry merupakan file maka ubah character menjadi lowercase. Proses perubahan menjadi lowercase dilakukan di dalam proses fungsi xmp_readdir.

4. jika entry merupakan directory maka ubah character menjadi uppercase. Proses perubahan menjadi uppercase dilakukan di dalam proses fungsi xmp_readdir.

Setelah melalui serangkaian filtering nama entry maka di akhir fungsi akan memanggil fungsi xmp_readdir kembali secara rekursif jika entry merupakan directory. Serta terakhir pemanggilan filler untuk memberi informasi entry pada mount point.

```
static int xmp_open(const char *path, struct fuse_file_info *fi) {
    (void) fi;

    if (strcmp(path, "/") == 0) {
        path = SOURCE_DIR;
    } else {
        char fpath[MAX_PATH*2];
        sprintf(fpath, "%s%s", SOURCE_DIR, path);

        struct stat st;
        if (lstat(fpath, &st) == 0 && S_ISREG(st.st_mode)) {
            char inPW[100];
            printf("Enter your Password: ");
            fflush(stdout);
            fgets(inPW, sizeof(inPW), stdin);

            int len = strlen(inPW);
            if (len > 0) inPW[len - 1] = '\0';

            while (strcmp(inPW, PASSWORD) != 0) {
                printf("The password you just entered is incorrect! Please try again.\n\nEnter your Password: ");
                fflush(stdout);
                fgets(inPW, sizeof(inPW), stdin);

                len = strlen(inPW);
                if (len > 0) inPW[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```

Berikutnya fungsi xmp_open, fungsi ini terdapat sebuah modifikasi dimana jika entry merupakan file maka akan memunculkan prompt password pada terminal FUSE yang dijalankan. Jika password yang dimasukkan tidak valid maka akan terus memunculkan prompt password tersebut.

```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[MAX_PATH*2];
    if(strcmp(path,"/") == 0){
        path=SOURCE_DIR;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",SOURCE_DIR,path);

    int res = 0;
    int fileDIr = 0 ;

    (void) fi;

    fileDIr = open(fpath, O_RDONLY);

    if (fileDIr == -1) return -errno;

    res = pread(fileDIr, buf, size, offset);

    if (res == -1) res = -errno;

    close(fileDIr);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    int fileDIr;
    int res;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    (void)fi;
    fileDIr = open(fpath, O_WRONLY);
    if (fileDIr == -1)
        return -errno;

    res = pwrite(fileDIr, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fileDIr);
    return res;
}
```

Selanjutnya terdapat beberapa fungsi standar xmp_read, xmp_write, dan xmp_mkdir untuk menyediakan fungsi read file, write file, dan mkdir pada mount point.

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir, // add new
    .write = xmp_write, // add new
    .open = xmp_open,
};
```

Berikutnya fuse_operations xmp_oper untuk assign fungsi-fungsi yang telah dijelaskan sebelumnya pada operasi-operasi yang tersedia pada FUSE mount point.

```
int main(int  argc, char *argv[]) {
  mkdir("mod", 0777);
  pid_t child_id;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    pid_t child_id1;

    child_id1 = fork();

    if (child_id1 < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id1 == 0) {
      char *argv[] = {"wget", "-O", "inifolderetc.zip", "drive.google.com/u/3/uc?id=1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT&export=download&confirm=yes", NULL};
      execv("/bin/wget", argv);
    }
    else{
      wait(NULL);
      char *argv[] = {"unzip", "inifolderetc.zip", NULL};
      execv("/usr/bin/unzip", argv);
    }

    
  } else {

    wait(NULL);

    umask(0);

    char cwd[1000];
    
    getcwd(cwd, sizeof(cwd));
    snprintf(SOURCE_DIR, sizeof(SOURCE_DIR), "%s/inifolderetc/sisop", cwd);
    snprintf(MOUNT_POINT, sizeof(MOUNT_POINT), "%s/mod", cwd);

    return fuse_main(argc, argv, &xmp_oper, NULL);
  }
}
```

Terakhir fungsi main, pada fungsi main terdapat 2 percabangan fork, fork pertama untuk membedakan tahap persiapan dan tahap FUSE. Tahap persiapan memiliki fork untuk download file zip pada child process dan unzip file yang telah didownload pada parent process yang wait. Setelah tahap persiapan selesai maka dilanjutkan dengan setup FUSE yang sebelumnya dilakukan fiksasi path SOURCE_DIR dan MOUNT_POINT menggunakan getcwd.

## docker-compose.yml

```
version: "3"

services:
  dhafin:
    build:
      context: .
      dockerfile: Dockerfile-dhafin
    container_name: Dhafin #nama docker container
    volumes:
      - /home/meng/praktikum/modul4/dockerTest:/etc/dhafin # nempatin /etc/dhafin di directory sebelah kiri ':' 
    command: tail -f /dev/null # biar tetep run setelah exit(0)

  normal:
    image: ubuntu:bionic 
    container_name: Normal # Nama docker container
    volumes:
      - /home/meng/praktikum/modul4/dockerTest:/etc/normal # nempatin /etc/normal di directory sebelah kiri ':' 
    command: tail -f /dev/null  # biar tetep run setelah exit(0)
```

Berikut merupakan file docker-compose yang digunakan untuk membuat docker container Dhafin dan normal. Pada container Dhafin menggunakan dockerfile khusus untuk install library dan run program yang dibutuhkan. Pada masing-masing container terdapat command ```tail -f /dev/null``` yang menjadikan container tidak exit walau program berhasil dieksekusi.
